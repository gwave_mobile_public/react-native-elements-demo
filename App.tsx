import React, {useState} from 'react';
import {Appearance, ScrollView} from 'react-native';
import {
  createTheme,
  useThemeMode,
  ThemeProvider,
  Text,
  Button,
  Image,
  Avatar,
  Dialog,
  makeStyles,
  ListItem,
} from '@rneui/themed';
import {Stack} from '@rneui/layout';
declare module '@rneui/themed' {
  export interface Colors {
    tertiary: string;
    accent: string;
    surface: string;
  }
  export interface TextProps {
    bold?: boolean;
    center?: boolean;
  }
  export interface ComponentTheme {
    Text: Partial<TextProps>;
  }
}

const theme = createTheme({
  mode: Appearance.getColorScheme() || 'light',
  lightColors: {
    accent: '#f00',
  },
  darkColors: {
    primary: 'green',
    accent: '#999',
  },
  components: {
    Button: {
      radius: 'xs',
    },
    Text: props => ({
      style: {
        fontWeight: props.bold ? 'bold' : 'normal',
        textAlign: props.center ? 'center' : 'left',
      },
    }),
  },
});

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <Component />
    </ThemeProvider>
  );
}

const Component = () => {
  const styles = useStyles();
  const [visible, setVisible] = useState(false);
  const {mode, setMode} = useThemeMode();
  const toggleTheme = () => {
    setMode(mode === 'light' ? 'dark' : 'light');
  };

  const toggleDialog = () => {
    setVisible(!visible);
  };

  const userlist = [
    {
      name: 'Amy Farha',
      avatar_url: 'https://uifaces.co/our-content/donated/XdLjsJX_.jpg',
      subtitle: 'amy.farha@gmail.com',
    },
    {
      name: 'Chris Jackson',
      avatar_url: 'https://uifaces.co/our-content/donated/KtCFjlD4.jpg',
      subtitle: 'cjackson@gmail.com',
    },
  ];
  return (
    <ScrollView contentContainerStyle={styles.box}>
      <Image source={{uri}} style={styles.image} />
      <Text h4 center style={styles.text}>
        Cross Platform React Native UI Toolkit
      </Text>
      <Text center>Normal Text</Text>
      <Text bold center>
        Bold Text
      </Text>
      <Button type="solid" title="Switch Theme" onPress={toggleTheme} />

      <Stack row align="center" spacing={10}>
        <Button>Primary</Button>
        <Button color="secondary" radius="lg" loading>
          Secondary
        </Button>
        <Button color="warning" loading>
          Warning
        </Button>
        <Button color="error" loading>
          Error
        </Button>
      </Stack>

      <Stack row align="center" spacing={10}>
        <Button
          title="HOME"
          titleStyle={styles.titleStyle}
          buttonStyle={styles.buttonStyle}
          containerStyle={styles.containerStyle}
        />
      </Stack>

      <Stack row spacing={4}>
        <Avatar
          size={32}
          rounded
          source={{uri: 'https://randomuser.me/api/portraits/men/36.jpg'}}
        />
        <Avatar
          size={32}
          rounded
          source={{uri: 'https://randomuser.me/api/portraits/men/35.jpg'}}
        />
      </Stack>
      <Button
        title="Open Buttonless Dialog"
        onPress={toggleDialog}
        buttonStyle={styles.button}
      />

      <Dialog isVisible={visible} onBackdropPress={toggleDialog}>
        <Dialog.Title title="Choose Account" />
        {userlist.map((l, i) => (
          <ListItem
            key={i}
            containerStyle={{
              marginHorizontal: -10,
              borderRadius: 8,
            }}
            onPress={toggleDialog}>
            <Avatar rounded source={{uri: l.avatar_url}} />
            <ListItem.Content>
              <ListItem.Title style={{fontWeight: '700'}}>
                {l.name}
              </ListItem.Title>
              <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        ))}
      </Dialog>
    </ScrollView>
  );
};

const useStyles = makeStyles(({colors}) => ({
  box: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexGrow: 1,
    padding: 10,
    backgroundColor: colors.background,
  },
  button: {
    borderRadius: 6,
    width: 220,
    margin: 20,
  },
  image: {width: 200, height: 200},
  text: {color: colors.accent},
  titleStyle: {fontWeight: '700'},
  buttonStyle: {
    backgroundColor: 'rgba(111, 202, 186, 1)',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 5,
    paddingVertical: 5,
  },
  containerStyle: {
    width: 200,
    height: 40,
    marginHorizontal: 50,
    marginVertical: 10,
  },
}));

const uri =
  'https://user-images.githubusercontent.com/5962998/65694309-a825f000-e043-11e9-8382-db0dba0851e3.png';
